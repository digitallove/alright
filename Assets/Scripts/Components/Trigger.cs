﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Trigger : IComponentData {

	#region Attributes

	public TriggerType type;
	public double cooldown;
	public float value;
	public Entity particleEmitter;
	[HideInInspector] public double timestamp;
	[HideInInspector] public bool hasBeenTriggered;

	public bool HasEverBeenTriggered {
		get {
			return !timestamp.Equals (0);
		}
	}
	public bool IsOneUseTrigger {
		get {
			return cooldown.Equals (-1);
		}
	}

	#endregion

	#region Public Behaviour

	public bool IsReady (double elapsedTime) {
		return !hasBeenTriggered && elapsedTime > timestamp + cooldown;
	}

	#endregion
}
