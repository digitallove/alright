﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Lifetime : IComponentData {
	public float value;
	[HideInInspector] public double timestamp;
}