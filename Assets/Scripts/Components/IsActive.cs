﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct IsActive : IComponentData {
	public bool value;
}
