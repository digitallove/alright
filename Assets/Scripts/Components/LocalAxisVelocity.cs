﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct LocalAxisVelocity : IComponentData {
	public float3 value;
	[HideInInspector] public float3 initialValue;
}
