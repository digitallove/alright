﻿using Unity.Entities;

public struct Play : IComponentData {

	#region Attributes

	public int totalCollectableCount;
	public int collectableCount;
	public bool isGameOver;
	public float health;

	public bool IsLevelComplete {
		get {
			return totalCollectableCount != 0 && collectableCount >= totalCollectableCount;
		}
	}

	#endregion

	#region Public Behaviour

	public void SetTotalCollectableCount (int count) {
		totalCollectableCount = count;
	}

	public void SetCollectableCount (int count) {
		collectableCount = count;
	}

	public void IncreaseCollectableCount (int value = 1) {
		collectableCount += value;
	}

	#endregion

}