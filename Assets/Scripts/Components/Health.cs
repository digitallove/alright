﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct Health : IComponentData {
	public float value;
	public bool IsAlive {
		get {
			return value > 0;
		}
	}
}