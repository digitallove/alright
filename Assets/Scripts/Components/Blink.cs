﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Blink : IComponentData {
	public double time;
	[HideInInspector] public bool isDisabled;
	[HideInInspector] public double timestamp;
}