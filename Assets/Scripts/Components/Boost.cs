﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct Boost : IComponentData {
	public double lifetime;
	public double timestamp;
	public float multiplier;
}