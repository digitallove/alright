﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Rotate : IComponentData {
	public float speedX;
	public float speedY;
	public float speedZ;
}