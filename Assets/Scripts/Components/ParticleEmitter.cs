﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct ParticleEmitter : IComponentData {

	[Header ("General")]
	public Entity prefab;
	public int particlesPerSec;

	[Header ("Lifetime")]
	public float minLifetime;
	public float maxLifetime;

	[Header ("Position")]
	public float3 emissionRadius;

	[Header ("Scale")]
	public float minScale;
	public float maxScale;
	public float lifetimeScaleIncrement;
	public AnimationCurve scaleAnimationCurve;

	[Header ("Velocity")]
	public float3 minVelocity;
	public float3 maxVelocity;

}