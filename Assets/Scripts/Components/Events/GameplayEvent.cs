﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct GameplayEvent : IComponentData {
	public TriggerType triggerType;
	public float value;
}