﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct ScaleTo : IComponentData {
	public float finalValue;
	public AnimationCurve animationCurve;
	[HideInInspector] public float initialValue;
}