﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Physics;

[GenerateAuthoringComponent]
public struct Attractable : IComponentData {
	public float value;
}