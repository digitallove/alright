﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Alright/GameData")]
public class GameData : ScriptableObject {
	public LevelType levelType;
}