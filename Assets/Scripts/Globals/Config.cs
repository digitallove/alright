﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public static class Config {
	public const int FrameRate = 60;
	public const float CameraSpeed = 5f;
	public const float CameraUpOffset = 20f;
	public const float InitialHealth = 1f;
	public const float BoostSecs = 1;
	public const float BoostMultiplier = 1.5f;
}