﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

public static class Constants {
	public readonly static float3 NullPosition = new float3 (999, 999, 999);
	public const float TriggerCooldownSecs = 1f;
}
