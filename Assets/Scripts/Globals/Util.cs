﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Physics;

public static class Util {

	#region ITriggerEventsJob

	public static Entity GetEntityFromComponentGroup<T> (Entity a, Entity b,
		ComponentDataFromEntity<T> group) where T : struct, IComponentData {
		if (group.Exists (a))
			return a;
		if (group.Exists (b))
			return b;
		return Entity.Null;
	}

	#endregion

	#region Scenes

	public static string GetLevelNameByIndex (int index) {
		return "L" + index.ToString ("000");
	}

	#endregion

}
