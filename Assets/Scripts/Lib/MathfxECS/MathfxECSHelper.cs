﻿using UnityEngine;
using Unity.Mathematics;
/// <summary>
/// A little helper for mathfx class.
/// </summary>
public static class MathfxECSHelper {

	#region MathfxECS

	/// <summary>
	/// Return value based on curve from MathfxECS class.
	/// </summary>
	public static float CurvedValueECS (AnimationCurve animationCurve, float start, float end, float t) {
		switch (animationCurve) {
			case AnimationCurve.Hermite:
				return MathfxECS.Hermite (start, end, t);
			case AnimationCurve.Sinerp:
				return MathfxECS.Sinerp (start, end, t);
			case AnimationCurve.Coserp:
				return MathfxECS.Coserp (start, end, t);
			case AnimationCurve.Berp:
				return MathfxECS.Berp (start, end, t);
			case AnimationCurve.Bounce:
				return start + ((end - start) * MathfxECS.Bounce (t));
			case AnimationCurve.Lerp:
				return MathfxECS.Lerp (start, end, t);
			case AnimationCurve.Clerp:
				return MathfxECS.CLerp (start, end, t);
			default:
				return 0;
		}
	}

	/// <summary>
	/// Return value based on curve from MathfxECS class.
	/// </summary>
	public static float2 CurvedValueECS (AnimationCurve animationCurve, float2 start, float2 end, float t) {
		return new float2 (CurvedValueECS (animationCurve, start.x, end.x, t), CurvedValueECS (animationCurve, start.y, end.y, t));
	}

	/// <summary>
	/// Return value based on curve from MathfxECS class.
	/// </summary>
	public static float3 CurvedValueECS (AnimationCurve animationCurve, float3 start, float3 end, float t) {
		return new float3 (CurvedValueECS (animationCurve, start.x, end.x, t), CurvedValueECS (animationCurve, start.y, end.y, t), CurvedValueECS (animationCurve, start.z, end.z, t));
	}

	#endregion

}