﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationCurve {
	Hermite,
	Sinerp,
	Coserp,
	Berp,
	Bounce,
	Lerp,
	Clerp
}