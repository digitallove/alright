﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KeyBehaviour : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	#region Attributes

	public virtual bool IsPressed {
		get {
			return isPressed;
		}
		set {
			isPressed = value;
		}
	}
	protected bool isPressed;

	#endregion

	#region Mono Behaviour

	protected virtual void OnEnable () {
		isPressed = false;
	}

	#endregion

	#region Public Behaviour

	public virtual void OnPointerDown (PointerEventData data) {
		isPressed = true;
	}

	public virtual void OnPointerUp (PointerEventData data) {
		isPressed = false;
	}

	#endregion


}
