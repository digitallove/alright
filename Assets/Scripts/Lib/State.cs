﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class State {

	#region Attributes

	protected StateMachine stateMachine;

	#endregion

	#region Public Behaviour

	public State (StateMachine stateMachine) {
		this.stateMachine = stateMachine;
	}

	public virtual void Enter () {
		AddListeners ();
	}

	public virtual void Exit () {
		RemoveListeners ();
	}

	#endregion

	#region Protected Behaviour

	protected virtual void AddListeners () {
	}

	protected virtual void RemoveListeners () {
	}

	#endregion

}
