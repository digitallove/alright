﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Player {

	#region Attributes

	public int sessionCount;
	public List<string> levels;
	public int currentLevelIndex;

	#endregion

	#region Static Behaviour

	public static Player Load () {
		Player player = JsonUtility.FromJson<Player> (PlayerPrefs.GetString (typeof (Player).ToString ()));
		if (player != null) {
			return player;
		} else {
			return new Player ();
		}
	}

	public static void Save (Player player) {
		PlayerPrefs.SetString (player.GetType ().ToString (), JsonUtility.ToJson (player));
	}

	#endregion

	#region Public Behaviour

	public Player () {
		this.sessionCount = 0;
		this.levels = new List<string> ();
	}

	public void IncreaseSessionCount () {
		sessionCount++;
	}

	public bool HasPassedLevel (string level) {
		foreach (string l in levels) {
			if (string.Equals (level, l))
				return true;
		}
		return false;
	}

	public bool AddLevel (string level) {
		if (HasPassedLevel (level)) {
			return false;
		} else {
			levels.Add (level);
			return true;
		}
	}

	public void SetCurrentLevelIndex (int value) {
		this.currentLevelIndex = value;
	}

	public void IncreaseCurrentLevelIndex () {
		SetCurrentLevelIndex (currentLevelIndex + 1);
	}

	#endregion

}
