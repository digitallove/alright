﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class StateMachine {

	#region Attributes

	public State currentState;
	private List<State> states;
	private List<Type> previousStates;

	#endregion

	#region Public Behaviour

	public StateMachine () {
		this.states = new List<State> ();
		this.previousStates = new List<Type> ();
	}

	public virtual void AddState (State state) {
		this.states.Add (state);
	}

	public virtual void RemoveState<T> () {
		State state = GetStateByType<T> ();
		if (state != null)
			states.Remove (state);
	}

	public virtual void ChangeState (Type type) {
		if (currentState != null) {
			previousStates.Add (currentState.GetType ());
			currentState.Exit ();
		}
		currentState = GetStateByType (type);
		currentState.Enter ();
	}

	public virtual void ChangeState<T> () {
		ChangeState (typeof (T));
	}

	public virtual void Return () {
		Type type = previousStates [previousStates.Count - 1];
		ChangeState (type);
	}

	#endregion

	#region Protected Behaviour

	protected virtual State GetStateByType (Type type) {
		foreach (State state in states) {
			if (state.GetType () == type)
				return state;
		}
		return null;
	}

	protected virtual State GetStateByType<T> () {
		return GetStateByType (typeof (T));
	}

	#endregion

}

