﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Scenes;
using System;
using Unity.Entities;

public class LevelController : MonoBehaviour {

	#region Attributes

	public SubScene [] subScenes;
	private SubSceneLoader subSceneLoader;
	public static event Action<LevelController> LoadEvent = (LevelController levelController) => { };

	#endregion

	#region Mono Behaviour

	private void Start () {
		subSceneLoader = World.DefaultGameObjectInjectionWorld.GetExistingSystem<SubSceneLoader> ();
		LoadEvent.Invoke (this);
		foreach (SubScene sub in subScenes) {
			subSceneLoader.LoadSubScene (sub);
		}
	}

	#endregion

}
