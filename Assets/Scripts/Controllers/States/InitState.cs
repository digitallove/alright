﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class InitState : BaseState {

	#region Attributes

	#endregion

	#region Public Behaviour

	public InitState (StateMachine stateMachine, GameData gameData, TransitionView transitionView) : base (stateMachine, gameData, transitionView) {
	}

	public override void Enter () {
		base.Enter ();
		stateMachine.ChangeState<LevelInitState> ();
	}

	public override void Exit () {
		base.Exit ();
	}

	#endregion

}
