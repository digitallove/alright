﻿using System.Collections.Generic;
using MEC;
using Unity.Entities;

public class LevelState : BaseState {

	#region Attributes

	private LevelView levelView;
	private PlaySystem playSystem;
	private CarSystem carSystem;

	#endregion

	#region Public Behaviour

	public LevelState (StateMachine stateMachine, GameData gameData, TransitionView transitionView, LevelView levelView) :
		base (stateMachine, gameData, transitionView) {
		this.levelView = levelView;
		this.levelView.Hide ();
		this.playSystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<PlaySystem> ();
		this.carSystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<CarSystem> ();
	}

	public override void Enter () {
		base.Enter ();
		levelView.RestartButtonClick += ToLevelInitState;
		Timing.RunCoroutine (PlayRoutine (), Segment.FixedUpdate, "PlayRoutine");
		carSystem.SetCarActive (true);
		levelView.Show ();
	}

	public override void Exit () {
		base.Exit ();
		levelView.RestartButtonClick -= ToLevelInitState;
		Timing.KillCoroutines ("PlayRoutine");
		carSystem.SetCarActive (false);
		levelView.Hide ();
	}

	public void ToLevelInitState () {
		stateMachine.ChangeState<LevelInitState> ();
	}

	#endregion

	#region Private Behaviour

	private IEnumerator<float> PlayRoutine () {
		while (!playSystem.play.IsLevelComplete && !playSystem.play.isGameOver) {
			levelView.ShowCollectables (playSystem.play.collectableCount, playSystem.play.totalCollectableCount);
			levelView.ShowHealth (playSystem.play.health);
			yield return Timing.WaitForOneFrame;
		}
		if (playSystem.play.IsLevelComplete) {
			stateMachine.ChangeState<LevelCompleteState> ();
		} else {
			stateMachine.ChangeState<GameOverState> ();
		}
	}

	#endregion


}
