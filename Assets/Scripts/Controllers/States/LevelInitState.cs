﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class LevelInitState : BaseState {

	#region Attributes

	private Player player;
	private LevelLoader levelLoader;
	private LevelInitView levelInitView;

	#endregion

	#region Public Behaviour

	public LevelInitState (StateMachine stateMachine, GameData gameData, TransitionView transitionView, Player player, LevelLoader levelLoader, LevelInitView levelInitView) :
		base (stateMachine, gameData, transitionView) {
		this.player = player;
		this.levelLoader = levelLoader;
		this.levelInitView = levelInitView;
		this.levelInitView.Hide ();
	}

	public override void Enter () {
		base.Enter ();
		LevelInitView.PlayButtonClick += ToLevelState;
		levelInitView.Show ();
		LoadLevel ();
	}

	public override void Exit () {
		base.Exit ();
		LevelInitView.PlayButtonClick -= ToLevelState;
		levelInitView.Hide ();
	}

	public void ToLevelState () {
		stateMachine.ChangeState<LevelState> ();
	}

	public virtual void LoadLevel () {
		if (levelLoader.HasLevel)
			levelLoader.UnloadLevel ();
		// Resets level index for testing purposes
		if (player.currentLevelIndex >= levelLoader.LevelCount)
			player.SetCurrentLevelIndex (0);
		levelLoader.LoadLevel (gameData, player.currentLevelIndex);
	}

	#endregion

}
