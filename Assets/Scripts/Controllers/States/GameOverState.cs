﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class GameOverState : BaseState {

	#region Attributes

	private GameOverView gameOverView;

	#endregion

	#region Public Behaviour

	public GameOverState (StateMachine stateMachine, GameData gameData, TransitionView transitionView, GameOverView gameOverView) :
		base (stateMachine, gameData, transitionView) {
		this.gameOverView = gameOverView;
		this.gameOverView.Hide ();
	}

	public override void Enter () {
		base.Enter ();
		GameOverView.RestartButtonClick += ToNextState;
		gameOverView.Show ();
	}

	public override void Exit () {
		base.Exit ();
		GameOverView.RestartButtonClick -= ToNextState;
		gameOverView.Hide ();
	}

	public void ToNextState () {
		transitionView.Show ();
		stateMachine.ChangeState<LevelInitState> ();
	}

	#endregion


}

