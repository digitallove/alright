﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class BaseState : State {

	#region Attributes

	protected GameData gameData;
	protected TransitionView transitionView;

	#endregion

	#region Public Behaviour

	public BaseState (StateMachine stateMachine, GameData gameData, TransitionView transitionView) : base (stateMachine) {
		this.gameData = gameData;
		this.transitionView = transitionView;
		this.transitionView.Hide ();
	}

	public override void Enter () {
		base.Enter ();
#if UNITY_EDITOR
		Debug.Log ("Enter " + this.GetType ());
#endif
	}

	public override void Exit () {
		base.Enter ();
#if UNITY_EDITOR
		Debug.Log ("Exit " + this.GetType ());
#endif
	}

	#endregion

}
