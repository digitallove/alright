﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class LevelCompleteState : BaseState {

	#region Attributes

	private Player player;
	private LevelCompleteView levelCompleteView;

	#endregion

	#region Public Behaviour

	public LevelCompleteState (StateMachine stateMachine, GameData gameData, TransitionView transitionView, Player player, LevelCompleteView levelCompleteView) :
		base (stateMachine, gameData, transitionView) {
		this.player = player;
		this.levelCompleteView = levelCompleteView;
		this.levelCompleteView.Hide ();
	}

	public override void Enter () {
		base.Enter ();
		LevelCompleteView.RestartButtonClick += ToNextLevel;
		levelCompleteView.Show ();
		player.AddLevel (Util.GetLevelNameByIndex (player.currentLevelIndex));
	}

	public override void Exit () {
		base.Exit ();
		LevelCompleteView.RestartButtonClick -= ToNextLevel;
		levelCompleteView.Hide ();
	}

	public void ToNextLevel () {
		player.IncreaseCurrentLevelIndex ();
		transitionView.Show ();
		stateMachine.ChangeState<LevelInitState> ();
	}

	#endregion


}

