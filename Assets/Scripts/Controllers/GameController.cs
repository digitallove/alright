﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class GameController : MonoBehaviour {

	#region Attributes

	[Header ("Data")]
	public GameData gameData;
	public Player player;

	[Header ("Scene")]
	public LevelLoader levelLoader;

	[Header ("UI")]
	public LevelInitView levelInitView;
	public LevelView levelView;
	public LevelCompleteView levelCompleteView;
	public GameOverView gameOverView;
	public TransitionView transitionView;

	private StateMachine stateMachine;

	#endregion

	#region Mono Behaviour

	private void Awake () {
		Application.targetFrameRate = Config.FrameRate;
		player = Player.Load ();
		player.IncreaseSessionCount ();
		player.currentLevelIndex = 0;
		stateMachine = new StateMachine ();
		stateMachine.AddState (new InitState (stateMachine, gameData, transitionView));
		stateMachine.AddState (new LevelInitState (stateMachine, gameData, transitionView, player, levelLoader, levelInitView));
		stateMachine.AddState (new LevelState (stateMachine, gameData, transitionView, levelView));
		stateMachine.AddState (new LevelCompleteState (stateMachine, gameData, transitionView, player, levelCompleteView));
		stateMachine.AddState (new GameOverState (stateMachine, gameData, transitionView, gameOverView));
	}

	private void Start () {
		stateMachine.ChangeState<InitState> ();
	}

	private void OnApplicationPause () {
		Player.Save (player);
	}

#if UNITY_EDITOR

	private void OnApplicationQuit () {
		Player.Save (player);
	}

#endif

	#endregion

}