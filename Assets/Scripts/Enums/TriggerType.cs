﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TriggerType {
	None,
	Collectable,
	Hazard,
	TurnPoint,
	Boost,
	Obstacle
}