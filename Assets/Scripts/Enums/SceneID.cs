﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneId {
	S000,
	S001,
	S002,
	S003,
	S004,
}