﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CollisionLayer {
	Player = 8,
	Collectable = 9,
	Hazard = 10
}