﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Scenes;
using Unity.Entities;

public class LevelLoader : MonoBehaviour {

	#region Attributes

	private SubScene[] subScenes;
	private SubSceneLoader subSceneLoader;
	private EntityLoader entityLoader;
	private LevelFactory levelFactory;
	private Entity levelSubScene;

	public int LevelCount {
		get {
			return subScenes.Length;
		}
	}
	public bool HasLevel {
		get {
			return levelSubScene != null;
		}
	}

	#endregion

	#region Mono Behaviour

	private void Awake () {
		subSceneLoader = World.DefaultGameObjectInjectionWorld.GetExistingSystem<SubSceneLoader> ();
		entityLoader = World.DefaultGameObjectInjectionWorld.GetExistingSystem<EntityLoader> ();
		levelFactory = World.DefaultGameObjectInjectionWorld.GetExistingSystem<LevelFactory> ();
		subScenes = GetComponentsInChildren<SubScene> ();
	}

	#endregion

	#region Public Behaviour

	public void LoadLevel (GameData gameData, int index) {
		entityLoader.LoadPlay ();
		Entity planet = entityLoader.LoadPlanet ();
		entityLoader.LoadCar ();
		switch (gameData.levelType) {
			case LevelType.Factory:
				levelFactory.Create (planet);
				break;
			case LevelType.Subscene:
				levelSubScene = subSceneLoader.LoadSubScene (subScenes[index]);
				break;
		}
	}

	public void UnloadLevel () {
		entityLoader.UnloadPlay ();
		entityLoader.UnloadPlanet ();
		entityLoader.UnloadCar ();
		if (levelSubScene != Entity.Null)
			subSceneLoader.UnloadSubScene (levelSubScene);
	}

	#endregion

}