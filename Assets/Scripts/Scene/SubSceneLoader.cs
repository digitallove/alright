﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Scenes;
using UnityEngine;

public class SubSceneLoader : ComponentSystem {

	#region Attributes

	private SceneSystem sceneSystem;

	#endregion

	#region Component System 

	protected override void OnCreate () {
		sceneSystem = World.GetOrCreateSystem<SceneSystem> ();
	}

	protected override void OnUpdate () {
	}

	#endregion

	#region Public Behaviour

	public Entity LoadSubScene (SubScene subScene) {
		return sceneSystem.LoadSceneAsync (subScene.SceneGUID);
	}

	public void UnloadSubScene (SubScene subScene) {
		sceneSystem.UnloadScene (subScene.SceneGUID);
	}

	public void UnloadSubScene (Entity entity) {
		sceneSystem.UnloadScene (entity);
	}

	#endregion

}