﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class LevelFactory : ComponentSystem {

	#region Component System

	protected override void OnUpdate () {
	}

	#endregion

	#region Public Behaviour

	public void Create (Entity planet) {
		DynamicBuffer<LinkedEntityGroup> buffer = EntityManager.GetBuffer<LinkedEntityGroup> (planet);
		foreach (LinkedEntityGroup group in buffer) {
			Entity child = group.Value;
			if (planet != child && EntityManager.HasComponent<SpawnPoint> (child)) {
				LocalToWorld localToWorld = EntityManager.GetComponentData<LocalToWorld> (child);
				Entity entity = EntityManager.Instantiate (PrefabContainer.Collectable);
				EntityManager.AddComponentData (entity, new LocalToWorld { Value = localToWorld.Value });
			}
		}
	}

	#endregion

}