﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class PrefabContainer : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity {

	#region Attributes

	public static Entity Car;
	public static Entity Planet;
	public static Entity Collectable;

	public GameObject carPrefab;
	public GameObject planetPrefab;
	public GameObject collectablePrefab;


	#endregion

	#region Public Behaviour

	public void DeclareReferencedPrefabs (List<GameObject> referencedPrefabs) {
		referencedPrefabs.Add (carPrefab);
		referencedPrefabs.Add (planetPrefab);
		referencedPrefabs.Add (collectablePrefab);
	}

	public void Convert (Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		Entity carEntity = conversionSystem.GetPrimaryEntity (carPrefab);
		PrefabContainer.Car = carEntity;
		Entity planetEntity = conversionSystem.GetPrimaryEntity (planetPrefab);
		PrefabContainer.Planet = planetEntity;
		Entity collectableEntity = conversionSystem.GetPrimaryEntity (collectablePrefab);
		PrefabContainer.Collectable = collectableEntity;
	}

	#endregion


}
