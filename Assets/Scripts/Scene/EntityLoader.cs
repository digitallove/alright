﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class EntityLoader : ComponentSystem {

	#region Attributes

	private Entity car;
	private Entity play;
	private Entity planet;

	#endregion

	#region Job Component System

	protected override void OnUpdate () {
	}

	#endregion

	#region Public Behaviour

	public Entity LoadCar () {
		car = EntityManager.Instantiate (PrefabContainer.Car);
		return car;
	}

	public void UnloadCar () {
		EntityManager.DestroyEntity (car);
	}

	public Entity LoadPlay () {
		play = EntityManager.CreateEntity ();
		EntityManager.AddComponentData (play, new Play {
			health = Config.InitialHealth
		});
		return play;
	}

	public void UnloadPlay () {
		EntityManager.DestroyEntity (play);
	}

	public Entity LoadPlanet () {
		planet = EntityManager.Instantiate (PrefabContainer.Planet);
		return planet;
	}

	public void UnloadPlanet () {
		EntityManager.DestroyEntity (planet);
	}

	#endregion

}
