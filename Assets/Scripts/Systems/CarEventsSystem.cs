﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Burst;
using Unity.Jobs;
using Unity.Physics;
using Unity.Collections;

public class CarEventsSystem : JobComponentSystem {

	#region Attributes

	private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

	#endregion

	#region Job Component System

	protected override void OnCreate () {
		endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem> ();
	}

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		Entity carEntity = GetSingletonEntity<Car> ();
		inputDeps = new TriggerEventsJob {
			entityCommandBuffer = endSimulationEntityCommandBufferSystem.CreateCommandBuffer ().ToConcurrent (),
			carEntity = carEntity,
			initialAngularVelocity = EntityManager.GetComponentData<AngularVelocity> (carEntity).value
		}.Schedule (this, inputDeps);
		endSimulationEntityCommandBufferSystem.AddJobHandleForProducer (inputDeps);
		return inputDeps;
	}

	[BurstCompile]
	private struct TriggerEventsJob : IJobForEachWithEntity<GameplayEvent> {

		public EntityCommandBuffer.Concurrent entityCommandBuffer;
		public Entity carEntity;
		public float3 initialAngularVelocity;

		public void Execute (Entity entity, int index, ref GameplayEvent gameplayEvent) {

			bool shouldTurn = false;
			bool hasBoost = false;

			switch (gameplayEvent.triggerType) {
				case TriggerType.TurnPoint:
					shouldTurn = true;
					break;
				case TriggerType.Boost:
					hasBoost = true;
					break;
			}

			entityCommandBuffer.DestroyEntity (index, entity);

			if (shouldTurn) {
				entityCommandBuffer.AddComponent (index, carEntity, new AngularVelocity { value = -initialAngularVelocity });
			}

			if (hasBoost) {
				entityCommandBuffer.AddComponent (index, carEntity, new Boost {
					lifetime = Config.BoostSecs,
					multiplier = Config.BoostMultiplier
				});
			}

		}

	}

	#endregion

}