﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Physics;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics.Extensions;

public class AttactionSystem : JobComponentSystem {

	#region Attributes

	private EntityQuery query;

	#endregion

	#region Job Component System

	protected override void OnCreate () {
		query = GetEntityQuery (new EntityQueryDesc {
			All = new ComponentType [] {
				ComponentType.ReadOnly<Attractor> (),
				ComponentType.ReadOnly<LocalToWorld> (),
			}
		});
	}

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		JobHandle jobHandle = new AttractionJob {
			origins = query.ToComponentDataArray<LocalToWorld> (Allocator.TempJob),
			attractors = query.ToComponentDataArray<Attractor> (Allocator.TempJob),
			elapsedTime = Time.ElapsedTime
		}.Schedule (this, inputDeps);
		return jobHandle;
	}

	[BurstCompile]
	private struct AttractionJob : IJobForEach<Attractable, LocalToWorld, PhysicsVelocity, PhysicsMass> {

		[ReadOnly] public NativeArray<LocalToWorld> origins;
		[ReadOnly] public NativeArray<Attractor> attractors;
		[ReadOnly] public double elapsedTime;

		public void Execute ([ReadOnly] ref Attractable attractable, ref LocalToWorld localToWorld, ref PhysicsVelocity physicsVelocity, ref PhysicsMass physicsMass) {
			float3 attraction = float3.zero;
			for (int i = 0; i < attractors.Length; i++) {
				attraction += ((origins [i].Position - localToWorld.Position) * attractors [i].value * attractable.value);
			}
			ComponentExtensions.ApplyLinearImpulse (ref physicsVelocity, physicsMass, attraction);
		}

	}

	#endregion

}
