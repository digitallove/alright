﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;

public class RotationSystem : JobComponentSystem {

	#region Component System

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		float deltaTime = Time.DeltaTime;
		return Entities.ForEach ((ref Rotate rotate, ref Rotation rotation) => {
			quaternion qx = quaternion.RotateX (deltaTime * rotate.speedX);
			quaternion qy = quaternion.RotateY (deltaTime * rotate.speedY);
			quaternion qz = quaternion.RotateZ (deltaTime * rotate.speedZ);
			rotation.Value = math.normalize (math.mul (qz, math.mul (qy, qx)));
		}).Schedule (inputDeps);
	}

	#endregion

}