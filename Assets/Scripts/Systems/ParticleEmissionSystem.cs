﻿using System.Diagnostics;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class ParticleEmissionSystem : JobComponentSystem {

	#region Attributes

	private BeginPresentationEntityCommandBufferSystem beginPresentationEntityCommandBufferSystem;
	private EntityQuery entityQuery;

	#endregion

	#region Job Component System

	protected override void OnCreate () {
		beginPresentationEntityCommandBufferSystem = World.GetOrCreateSystem<BeginPresentationEntityCommandBufferSystem> ();
		entityQuery = GetEntityQuery (
			new EntityQueryDesc () {
				All = new ComponentType [] {
					ComponentType.ReadOnly<ParticleEmitter> (),
					ComponentType.ReadOnly<LocalToWorld> (),
					ComponentType.ReadOnly<IsActive> ()
				}
			});
	}

	protected override JobHandle OnUpdate (JobHandle jobHandle) {

		NativeArray<Random> randomArray = World.GetExistingSystem<RandomSystem> ().RandomArray;

		jobHandle = new ParticleEmissionJob {
			entityCommandBuffer = beginPresentationEntityCommandBufferSystem.CreateCommandBuffer ().ToConcurrent (),
			particleEmitterType = GetArchetypeChunkComponentType<ParticleEmitter> (true),
			localToWorldType = GetArchetypeChunkComponentType<LocalToWorld> (true),
			isActiveType = GetArchetypeChunkComponentType<IsActive> (true),
			elapsedTime = Time.ElapsedTime,
			deltaTime = Time.DeltaTime,
			randomArray = randomArray
		}.Schedule (entityQuery, jobHandle);
		beginPresentationEntityCommandBufferSystem.AddJobHandleForProducer (jobHandle);

		return jobHandle;

	}

	[BurstCompile]
	private struct ParticleEmissionJob : IJobChunk {

		public EntityCommandBuffer.Concurrent entityCommandBuffer;
		[ReadOnly] public ArchetypeChunkComponentType<ParticleEmitter> particleEmitterType;
		[ReadOnly] public ArchetypeChunkComponentType<LocalToWorld> localToWorldType;
		[ReadOnly] public ArchetypeChunkComponentType<IsActive> isActiveType;
		public double elapsedTime;
		public double deltaTime;
		public NativeArray<Random> randomArray;

		public void Execute (ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {

			NativeArray<ParticleEmitter> particleEmitters = chunk.GetNativeArray (particleEmitterType);
			NativeArray<LocalToWorld> localToWorlds = chunk.GetNativeArray (localToWorldType);
			NativeArray<IsActive> isActives = chunk.GetNativeArray (isActiveType);
			Random random = randomArray [chunkIndex];

			for (int i = 0; i < chunk.Count; ++i) {

				ParticleEmitter particleEmitter = particleEmitters [i];
				LocalToWorld localToWorld = localToWorlds [i];
				IsActive isActive = isActives [i];

				float seed = random.NextFloat (0, 1);
				bool mustSpawn = isActive.value && seed < particleEmitter.particlesPerSec * deltaTime;

				// Particle creation
				if (mustSpawn) {

					Entity entity = entityCommandBuffer.Instantiate (chunkIndex, particleEmitter.prefab);

					// Lifetime
					float lifetime = random.NextFloat (particleEmitter.minLifetime, particleEmitter.maxLifetime);
					entityCommandBuffer.AddComponent (chunkIndex, entity, new Lifetime { timestamp = elapsedTime, value = lifetime });

					// Position
					float3 position = localToWorld.Position + random.NextFloat3 (-particleEmitter.emissionRadius, particleEmitter.emissionRadius);
					entityCommandBuffer.AddComponent (chunkIndex, entity, new Translation { Value = position });

					// Rotation
					entityCommandBuffer.AddComponent (chunkIndex, entity, new Rotation { Value = localToWorld.Rotation });

					// Size
					float scale = random.NextFloat (particleEmitter.minScale, particleEmitter.maxScale);
					entityCommandBuffer.AddComponent (chunkIndex, entity, new Scale { Value = scale });
					entityCommandBuffer.AddComponent (chunkIndex, entity, new ScaleTo {
						finalValue = (float) scale + particleEmitter.lifetimeScaleIncrement,
						initialValue = scale,
						animationCurve = particleEmitter.scaleAnimationCurve
					});

					// Velocity
					float3 velocity = new float3 (
						random.NextFloat (particleEmitter.minVelocity.x, particleEmitter.maxVelocity.x),
						random.NextFloat (particleEmitter.minVelocity.y, particleEmitter.maxVelocity.y),
						random.NextFloat (particleEmitter.minVelocity.z, particleEmitter.maxVelocity.z)
					);
					entityCommandBuffer.AddComponent (chunkIndex, entity, new LocalAxisVelocity {
						value = velocity,
						initialValue = velocity
					});

				}

				randomArray [chunkIndex] = random;

			}

		}

	}

	#endregion

}