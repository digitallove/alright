﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;

public class LifetimeSystem : JobComponentSystem {

	#region Attributes

	public EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

	#endregion

	#region Job Component System

	protected override void OnCreate () {
		endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem> ();
	}

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		JobHandle jobHandle = new LifetimeJob {
			entityCommandBuffer = endSimulationEntityCommandBufferSystem.CreateCommandBuffer ().ToConcurrent (),
			elapsedTime = Time.ElapsedTime
		}.Schedule (this, inputDeps);
		endSimulationEntityCommandBufferSystem.AddJobHandleForProducer (jobHandle);
		return jobHandle;
	}

	[BurstCompile]
	private struct LifetimeJob : IJobForEachWithEntity<Lifetime> {

		public EntityCommandBuffer.Concurrent entityCommandBuffer;
		public double elapsedTime;

		public void Execute (Entity entity, int index, ref Lifetime lifetime) {
			if (lifetime.timestamp != 0 && elapsedTime > lifetime.timestamp + lifetime.value)
				entityCommandBuffer.DestroyEntity (index, entity);
		}

	}

	#endregion

}
