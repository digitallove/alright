﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Mathematics;
using Unity.Transforms;

public class LocalAxisVelocitySystem : JobComponentSystem {

	#region Job Component System

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		return Entities.WithNone<Car> ().ForEach ((ref LocalAxisVelocity localAxisVelocity, ref PhysicsVelocity physicsVelocity, ref LocalToWorld localToWorld) => {
			float3 rightVelocity = localToWorld.Right * localAxisVelocity.value.x;
			float3 upVelocity = localToWorld.Up * localAxisVelocity.value.y;
			float3 forwardVelocity = localToWorld.Forward * localAxisVelocity.value.z;
			float3 velocity = rightVelocity + upVelocity + forwardVelocity;
			physicsVelocity.Linear = velocity;
		}).Schedule (inputDeps);
	}

	#endregion
}
