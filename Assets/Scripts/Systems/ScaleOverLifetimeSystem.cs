﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

public class ScaleOverLifetimeSystem : JobComponentSystem {

	#region Job Component System

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		double elapsedTime = Time.ElapsedTime;
		return Entities.ForEach ((ref Lifetime lifetime, ref ScaleTo scaleTo, ref Scale scale) => {
			float percentage = (float) (elapsedTime - lifetime.timestamp) / lifetime.value;
			scale.Value = MathfxECSHelper.CurvedValueECS (scaleTo.animationCurve, scaleTo.initialValue, scaleTo.finalValue, percentage);
		}).Schedule (inputDeps);
	}

	#endregion

}
