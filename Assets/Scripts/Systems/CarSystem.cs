﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Burst;
using Unity.Jobs;
using Unity.Physics;
using Unity.Collections;
using Unity.Physics.Extensions;

public class CarSystem : JobComponentSystem {

	#region Attributes

	private AngularVelocity previousAngularVelocity;

	#endregion

	#region Job Component System

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		SetTurnLights ();
		inputDeps = new MovementJob {
			hasInput = UIInput.turn
		}.Schedule (this, inputDeps);
		return inputDeps;
	}

	[BurstCompile]
	private struct MovementJob : IJobForEach<AngularVelocity, PhysicsVelocity, LocalToWorld, IsActive, LocalAxisVelocity> {

		[ReadOnly] public bool hasInput;

		public void Execute (ref AngularVelocity angularVelocity, ref PhysicsVelocity physicsVelocity, ref LocalToWorld localToWorld, ref IsActive isActive, ref LocalAxisVelocity localAxisVelocity) {
			if (isActive.value) {
				physicsVelocity.Linear = localToWorld.Forward * localAxisVelocity.value.z;
				physicsVelocity.Angular = hasInput ? angularVelocity.value : float3.zero;
			} else {
				physicsVelocity.Linear = float3.zero;
				physicsVelocity.Angular = float3.zero;
			}
		}

	}

	#endregion

	#region Public Behaviour

	public void SetCarActive (bool isActive) {

		// Car
		Entity entity = GetSingletonEntity<Car> ();
		EntityManager.AddComponentData (entity, new IsActive { value = isActive });

		// Children
		DynamicBuffer<LinkedEntityGroup> buffer = EntityManager.GetBuffer<LinkedEntityGroup> (entity);
		foreach (LinkedEntityGroup group in buffer) {
			Entity child = group.Value;
			if (entity != child && EntityManager.HasComponent<IsActive> (child) && !EntityManager.HasComponent<Blink> (child)) {
				EntityManager.AddComponentData (child, new IsActive { value = isActive });
			}
		}

	}

	#endregion

	#region Public Behaviour

	private void SetTurnLights () {

		Entity entity = GetSingletonEntity<Car> ();
		AngularVelocity angularVelocity = EntityManager.GetComponentData<AngularVelocity> (entity);
		if (previousAngularVelocity.value.Equals (float3.zero))
			previousAngularVelocity = angularVelocity;

		if (angularVelocity.value.y != previousAngularVelocity.value.y) {

			// Children
			DynamicBuffer<LinkedEntityGroup> buffer = EntityManager.GetBuffer<LinkedEntityGroup> (entity);
			foreach (LinkedEntityGroup group in buffer) {
				Entity child = group.Value;
				if (entity != child && EntityManager.HasComponent<IsActive> (child) && EntityManager.HasComponent<Blink> (child)) {
					IsActive isActive = EntityManager.GetComponentData<IsActive> (child);
					EntityManager.AddComponentData (child, new IsActive { value = !isActive.value });
				}
			}

			previousAngularVelocity = angularVelocity;

		}

	}

	#endregion

}