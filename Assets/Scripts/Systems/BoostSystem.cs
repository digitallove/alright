﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;

public class BoostSystem : JobComponentSystem {

	#region Attributes

	private BeginInitializationEntityCommandBufferSystem beginInitializationEntityCommandBufferSystem;

	#endregion

	#region JobComponentSystem

	protected override void OnCreate () {
		beginInitializationEntityCommandBufferSystem = World.GetExistingSystem<BeginInitializationEntityCommandBufferSystem> ();
	}

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		return new BoostJob {
			entityCommandBuffer = beginInitializationEntityCommandBufferSystem.CreateCommandBuffer ().ToConcurrent (),
			elapsedTime = Time.ElapsedTime
		}.Schedule (this, inputDeps);
	}

	[BurstCompile]
	private struct BoostJob : IJobForEachWithEntity<Car, Boost, Attractable, LocalAxisVelocity> {

		public EntityCommandBuffer.Concurrent entityCommandBuffer;
		public double elapsedTime;

		public void Execute (Entity entity, int index, [ReadOnly] ref Car car, ref Boost boost, ref Attractable attractable, ref LocalAxisVelocity localAxisVelocity) {
			if (boost.timestamp == 0) {
				boost.timestamp = elapsedTime;
				localAxisVelocity.value = localAxisVelocity.value * boost.multiplier;
				attractable.value = attractable.value * boost.multiplier;
			}
			if (elapsedTime >= boost.timestamp + boost.lifetime) {
				localAxisVelocity.value = localAxisVelocity.value / boost.multiplier;
				attractable.value = attractable.value / boost.multiplier;
				entityCommandBuffer.RemoveComponent<Boost> (index, entity);
			}
		}

	}

	#endregion

}
