﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;
using Unity.Mathematics;

public class PlaySystem : JobComponentSystem {

	#region Attributes

	public Play play;
	private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

	#endregion

	#region Job Component System

	protected override void OnCreate () {
		endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem> ();
	}

	protected override JobHandle OnUpdate (JobHandle inputDeps) {

		Entity playEntity = GetSingletonEntity<Play> ();
		play = EntityManager.GetComponentData<Play> (playEntity);

		if (play.totalCollectableCount == 0) {
			SetTotalCollectableCount (playEntity, play);
		} else {
			Entity carEntity = GetSingletonEntity<Car> ();
			inputDeps = new TriggerEventsJob {
				entityCommandBuffer = endSimulationEntityCommandBufferSystem.CreateCommandBuffer ().ToConcurrent (),
				playEntity = playEntity,
				play = play
			}.Schedule (this, inputDeps);
		}
		endSimulationEntityCommandBufferSystem.AddJobHandleForProducer (inputDeps);

		return inputDeps;

	}

	[BurstCompile]
	private struct TriggerEventsJob : IJobForEachWithEntity<GameplayEvent> {

		public EntityCommandBuffer.Concurrent entityCommandBuffer;
		public Entity playEntity;
		public Play play;

		public void Execute (Entity entity, int index, ref GameplayEvent gameplayEvent) {

			int collectedCount = 0;
			float damage = 0;

			switch (gameplayEvent.triggerType) {
				case TriggerType.Collectable:
					collectedCount++;
					break;
				case TriggerType.Hazard:
					damage += gameplayEvent.value;
					break;
				case TriggerType.Obstacle:
					damage += gameplayEvent.value;
					break;
			}

			entityCommandBuffer.DestroyEntity (index, entity);

			if (collectedCount > 0) {
				play.IncreaseCollectableCount (collectedCount);
				entityCommandBuffer.AddComponent (index, playEntity, play);
			}

			if (damage > 0) {
				play.health -= damage;
				if (play.health <= 0)
					play.isGameOver = true;
				entityCommandBuffer.AddComponent (index, playEntity, play);
			}

		}

	}

	#endregion

	#region Private Behaviour

	private void SetTotalCollectableCount (Entity entity, Play play) {
		int totalCollectableCount = 0;
		Entities.ForEach ((ref Trigger trigger) => {
			if (trigger.type == TriggerType.Collectable)
				totalCollectableCount++;
		}).Run ();
		play.SetTotalCollectableCount (totalCollectableCount);
		EntityManager.AddComponentData (entity, play);
	}

	#endregion

}