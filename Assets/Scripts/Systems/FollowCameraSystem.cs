﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Unity.Physics;

[UpdateAfter (typeof (CarSystem))]
public class FollowCameraSystem : ComponentSystem {

	#region Attributes

	private Transform cam;
	private Vector3 initialPosition;
	private Quaternion initialRotation;

	#endregion

	#region Component System

	protected override void OnStartRunning () {
		cam = Camera.main.transform;
		initialPosition = cam.position;
		initialRotation = cam.rotation;
	}

	protected override void OnUpdate () {
		Entity target = GetSingletonEntity<Car> ();
		if (target != null) {
			Play play = GetSingleton<Play> ();
			LocalToWorld localToWorld = EntityManager.GetComponentData<LocalToWorld> (target);
			IsActive isCarActive = EntityManager.GetComponentData<IsActive> (target);
			if (!play.isGameOver && !play.IsLevelComplete && isCarActive.value) {
				cam.position = math.lerp (cam.position, localToWorld.Up * Config.CameraUpOffset, Config.CameraSpeed * Time.DeltaTime);
				cam.LookAt (localToWorld.Position, localToWorld.Up);
				Quaternion targetRotation = Quaternion.LookRotation (((Vector3) localToWorld.Position) - cam.position, localToWorld.Up);
				cam.rotation = Quaternion.Slerp (cam.rotation, targetRotation, Config.CameraSpeed * Time.DeltaTime);
			} else {
				cam.position = Vector3.Lerp (cam.position, initialPosition, Config.CameraSpeed * Time.DeltaTime);
				cam.rotation = Quaternion.Slerp (cam.rotation, initialRotation, Config.CameraSpeed * Time.DeltaTime);
			}
		} else {
			cam.position = Vector3.Lerp (cam.position, initialPosition, Config.CameraSpeed * Time.DeltaTime);
			cam.rotation = Quaternion.Slerp (cam.rotation, initialRotation, Config.CameraSpeed * Time.DeltaTime);
		}
	}

	#endregion

}