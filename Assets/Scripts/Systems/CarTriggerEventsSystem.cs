﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
using Unity.Transforms;

[UpdateAfter (typeof (EndFramePhysicsSystem))]
public class CarTriggerEventsSystem : JobComponentSystem {

	#region Attributes

	private BuildPhysicsWorld buildPhysicsWorldSystem;
	private StepPhysicsWorld stepPhysicsWorldSystem;

	#endregion

	#region Job Component System

	protected override void OnCreate () {
		buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld> ();
		stepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld> ();
	}

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		JobHandle jobHandle = new TriggerEventsJob {
			carGroup = GetComponentDataFromEntity<Car> (true),
			triggerGroup = GetComponentDataFromEntity<Trigger> (),
			elapsedTime = Time.ElapsedTime
		}.Schedule (stepPhysicsWorldSystem.Simulation, ref buildPhysicsWorldSystem.PhysicsWorld, inputDeps);
		return jobHandle;
	}

	[BurstCompile]
	private struct TriggerEventsJob : ITriggerEventsJob {

		[ReadOnly] public ComponentDataFromEntity<Car> carGroup;
		public ComponentDataFromEntity<Trigger> triggerGroup;
		[ReadOnly] public double elapsedTime;

		public void Execute (TriggerEvent triggerEvent) {

			Entity a = triggerEvent.Entities.EntityA;
			Entity b = triggerEvent.Entities.EntityB;

			Entity carEntity = Util.GetEntityFromComponentGroup (a, b, carGroup);
			Entity triggerEntity = Util.GetEntityFromComponentGroup (a, b, triggerGroup);

			if (carEntity != Entity.Null && triggerEntity != Entity.Null) {
				Trigger trigger = triggerGroup [triggerEntity];
				if (trigger.IsReady (elapsedTime)) {
					trigger.hasBeenTriggered = true;
					trigger.timestamp = elapsedTime;
					triggerGroup [triggerEntity] = trigger;
				}
			}

		}

	}

	#endregion

}