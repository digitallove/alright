﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs.LowLevel.Unsafe;
using Unity.Mathematics;

// Ref. https://reeseschultz.com/random-number-generation-with-unity-dots/

[UpdateInGroup (typeof (InitializationSystemGroup))]
class RandomSystem : ComponentSystem {

	#region Attributes

	public NativeArray<Random> RandomArray {
		get; private set;
	}

	#endregion

	#region ComponentSystem

	protected override void OnCreate () {
		Random [] randomArray = new Random [JobsUtility.MaxJobThreadCount];
		System.Random seed = new System.Random ();
		for (int i = 0; i < JobsUtility.MaxJobThreadCount; ++i) {
			randomArray [i] = new Random ((uint) seed.Next ());
		}
		RandomArray = new NativeArray<Random> (randomArray, Allocator.Persistent);
	}

	protected override void OnUpdate () {
	}

	protected override void OnDestroy () {
		RandomArray.Dispose ();
	}

	#endregion

}