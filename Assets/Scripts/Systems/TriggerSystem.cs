﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

public class TriggerSystem : JobComponentSystem {

	#region Attributes

	private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

	#endregion

	#region Job Component System

	protected override void OnCreate () {
		endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem> ();
	}

	protected override JobHandle OnUpdate (JobHandle inputDependencies) {
		JobHandle jobHandle = new TriggerJob {
			entityCommandBuffer = endSimulationEntityCommandBufferSystem.CreateCommandBuffer ().ToConcurrent (),
			elapsedTime = Time.ElapsedTime
		}.Schedule (this, inputDependencies);
		endSimulationEntityCommandBufferSystem.AddJobHandleForProducer (jobHandle);
		return jobHandle;
	}

	[BurstCompile]
	private struct TriggerJob : IJobForEachWithEntity<Trigger, Translation> {

		public EntityCommandBuffer.Concurrent entityCommandBuffer;
		[ReadOnly] public double elapsedTime;

		public void Execute (Entity entity, int jobIndex, ref Trigger trigger, ref Translation translation) {

			if (trigger.hasBeenTriggered) {

				// Trigger
				trigger.timestamp = elapsedTime;
				if (trigger.IsOneUseTrigger) {
					entityCommandBuffer.AddComponent<Disabled> (jobIndex, entity);
				} else {
					trigger.hasBeenTriggered = false;
				}

				// Play Event
				entityCommandBuffer.AddComponent (jobIndex, entityCommandBuffer.CreateEntity (jobIndex),
					new GameplayEvent {
						triggerType = trigger.type,
						value = trigger.value
					});

				// VFX
				if (trigger.particleEmitter != Entity.Null) {
					Entity particleEmitter = entityCommandBuffer.Instantiate (jobIndex, trigger.particleEmitter);
					entityCommandBuffer.AddComponent (jobIndex, particleEmitter, new Translation { Value = translation.Value });
					entityCommandBuffer.AddComponent (jobIndex, particleEmitter, new Lifetime { timestamp = elapsedTime, value = 0.1f });
				}

			}

		}

	}

	#endregion

}
