﻿using Unity.Entities;
using Unity.Collections;
using Unity.Jobs;
using Unity.Burst;

public class BlinkSystem : JobComponentSystem {

	#region Attributes

	private BeginPresentationEntityCommandBufferSystem beginPresentationEntityCommandBufferSystem;
	private EntityQuery query;

	#endregion

	#region Job Component System

	protected override void OnCreate () {
		beginPresentationEntityCommandBufferSystem = World.GetOrCreateSystem<BeginPresentationEntityCommandBufferSystem> ();
		query = GetEntityQuery (new EntityQueryDesc {
			All = new ComponentType[] { typeof (Blink) },
			Options = EntityQueryOptions.IncludeDisabled
		});
	}

	protected override JobHandle OnUpdate (JobHandle inputDeps) {
		inputDeps = new BlinkJob {
			entityCommandBuffer = beginPresentationEntityCommandBufferSystem.CreateCommandBuffer ().ToConcurrent (),
			entityType = GetArchetypeChunkEntityType (),
			blinkType = GetArchetypeChunkComponentType<Blink> (),
			isActiveType = GetArchetypeChunkComponentType<IsActive> (),
			elapsedTime = Time.ElapsedTime
		}.Schedule (query, inputDeps);
		beginPresentationEntityCommandBufferSystem.AddJobHandleForProducer (inputDeps);
		return inputDeps;
	}

	[BurstCompile]
	private struct BlinkJob : IJobChunk {

		public EntityCommandBuffer.Concurrent entityCommandBuffer;
		[ReadOnly] public ArchetypeChunkEntityType entityType;
		[ReadOnly] public ArchetypeChunkComponentType<Blink> blinkType;
		[ReadOnly] public ArchetypeChunkComponentType<IsActive> isActiveType;
		public double elapsedTime;

		public void Execute (ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {

			NativeArray<Entity> entities = chunk.GetNativeArray (entityType);
			NativeArray<Blink> blinks = chunk.GetNativeArray (blinkType);
			NativeArray<IsActive> isActives = chunk.GetNativeArray (isActiveType);

			for (int i = 0; i < chunk.Count; ++i) {

				Entity entity = entities[i];
				Blink blink = blinks[i];
				IsActive isActive = isActives[i];

				if (!isActive.value) {
					blink.isDisabled = true;
				} else {
					if (blink.timestamp + blink.time <= elapsedTime) {
						blink.isDisabled = !blink.isDisabled;
						blink.timestamp = elapsedTime;
					}
				}

				entityCommandBuffer.AddComponent (chunkIndex, entity, blink);
				if (blink.isDisabled) {
					entityCommandBuffer.RemoveComponent<Disabled> (chunkIndex, entity);
				} else {
					entityCommandBuffer.AddComponent<Disabled> (chunkIndex, entity);
				}

			}

		}

	}

	#endregion

}
