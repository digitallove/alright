﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseView : MonoBehaviour {

	#region Attributes

	private Canvas canvas;

	public virtual bool IsInit {
		get {
			return canvas != null;
		}
	}
	public virtual bool IsActive {
		get {
			return canvas.enabled;
		}
	}

	#endregion

	#region Public Behaviour

	public virtual void Init () {
		this.canvas = GetComponent<Canvas> ();
	}

	public virtual void Show () {
		if (!IsInit)
			Init ();
		canvas.enabled = true;
	}

	public virtual void Hide () {
		if (!IsInit)
			Init ();
		canvas.enabled = false;
	}

	#endregion

}
