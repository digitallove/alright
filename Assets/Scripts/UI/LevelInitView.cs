﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LevelInitView : BaseView {

	#region Attributes

	public Button playButton;
	public static event Action PlayButtonClick = () => { };

	#endregion

	#region Mono Behaviour

	private void Awake () {
		playButton.onClick.AddListener (() => PlayButtonClick.Invoke ());
	}

	#endregion

}
