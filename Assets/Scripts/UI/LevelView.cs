﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class LevelView : BaseView {

	#region Attributes

	public TextMeshProUGUI collectablesLabel;
	public Image healthImage;
	public Button restartButton;
	public KeyBehaviour turnButton;

	public event Action RestartButtonClick = () => { };

	#endregion

	#region Mono Behaviour

	private void Awake () {
		restartButton.onClick.AddListener (InvokeRestartButtonClick);
	}

	private void Update () {
		UIInput.turn = turnButton.IsPressed;
	}

	private void OnDestroy () {
		restartButton.onClick.RemoveListener (InvokeRestartButtonClick);
	}

	#endregion

	#region Public Behaviour

	public override void Show () {
		base.Show ();
		turnButton.IsPressed = false;
	}

	public void ShowCollectables (int collectableCount, int totalCollectableCount) {
		collectablesLabel.text = collectableCount.ToString () + " / " + totalCollectableCount.ToString ();
	}

	public void ShowHealth (float value) {
		healthImage.fillAmount = value;
	}

	public void InvokeRestartButtonClick () {
		RestartButtonClick.Invoke ();
	}

	#endregion

}