﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LevelCompleteView : BaseView {

	#region Attributes

	public Button restartButton;

	public static event Action RestartButtonClick = () => { };

	#endregion

	#region Mono Behaviour

	private void Awake () {
		this.restartButton.onClick.AddListener (() => RestartButtonClick.Invoke ());
	}

	#endregion

	#region Public Behaviour

	public override void Show () {
		base.Show ();
	}

	#endregion

}