﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TransitionView : BaseView {

	#region Attributes

	private Sequence sequence;
	private Image image;

	#endregion

	#region Public Behaviour

	public override void Init () {
		base.Init ();
		image = GetComponent<Image> ();
		image.DOFade (0, 0);
		sequence = DOTween.Sequence ()
			  .Append (image.DOFade (1, 0.2f))
			  .Append (image.DOFade (0, 0.4f))
			  .OnComplete (Hide);
		sequence.Pause ();
	}

	public override void Show () {
		base.Show ();
		sequence.Play ();
	}

	#endregion

}
